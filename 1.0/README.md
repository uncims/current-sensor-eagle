# README #

This is a simple PCB designed in CADsoft Eaglecad.

The board is intended to measure two current sources and report them over a serial or USB connection. To do this it uses:

* [Arduino Pro Micro](https://www.sparkfun.com/products/12587)
* [ADS1015](http://www.ti.com.cn/cn/lit/ds/symlink/ads1015.pdf)
* Two [Pololu Current Sensors](https://www.pololu.com/category/118/current-sensors)


This board has not yet been tested.